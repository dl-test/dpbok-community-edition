
[[KLP-securing-apps]]
==== Securing Applications and Digital Products

*Description*

Application security includes a broad range of specialized areas, including secure software design and development, threat modeling, vulnerability assessment, penetration testing, and the impact of security on DevOps (and _vice versa_). As with other aspects of security, the move to cloud computing brings some changes to application security. The CSA guidance on cloud security specifically addresses application security considerations in cloud environments in domain 10 of their latest cloud security guidance.

An important element of application security is Secure Software Development Lifecycle (SSDLC), an approach toward developing software in a secure manner. Numerous frameworks and resources are available to follow, including from Microsoft (Security Development Lifecycle), NIST (NIST SP 800-64 Rev. 2), ISO/IEC (ISO/IEC 27034-1:2011), and the Open Web Application Security Project (OWASP Top Ten). In addition, information resources available from MITRE including Common Weaknesses Enumeration (CWE) and Common Vulnerability and Exposures (CVE) are helpful to development teams striving to develop secure code. 

A basic approach to secure design and development will include these phases: Training – Define – Design – Develop – Test.footnote:[Cloud Security Alliance, Security Guidance for Critical Areas of Focus in Cloud Computing version 4] A component of an SSDLC is threat modeling. Good resources on threat modeling are available from Microsoft and from The Open Group.

It is worth noting that the move to cloud computing affects all aspects of an SSDLC, because cloud services abstract various computing resources, and there are automation approaches used in cloud services that fundamentally change the ways in which software is developed, tested, and deployed in cloud services _versus_ in on-premises computing. In addition, there are significant differences in the degree of visibility and control that is provided to the customer, including availability of system logs at various points in the computing stack.

Application security will also include secure deployment, encompassing code review, unit, regression, and functional testing, and static and dynamic application security testing.

Other key aspects of application security include vulnerability assessment and penetration testing. Both have differences in cloud _versus_ on-premises, as a customer’s ability to perform vulnerability scans and penetration tests may be restricted by contract by the CSP, and there may be technical issues relating to the type of cloud service, single _versus_ multi-tenancy of the application, and so on.

////
* Social engineering
* OAuth and related
* FOSS - Bitcoin miner example
* Code pipelines and continuous integration
** Static analysis
** Upstream tech products
** NIST CVE
////

*Evidence of Notability*

_To be added in a future version._

////
(references to early importance of this as cited
by security luminaries)
////

*Limitations*

_To be added in a future version._

////
(statement re. how organizations employing older security
approaches and standards may have a hard time adapting to the challenges
of securing applications generally, and in cloud services)
////

*Related Topics*

* https://www.microsoft.com/en-us/securityengineering/sdl[Security Development Lifecycle (Microsoft)]
* https://www.microsoft.com/en-us/securityengineering/sdl/threatmodeling[Threat Modeling (Microsoft)]
* https://publications.opengroup.org/g112[Open Enterprise Security Architecture (O-ESA) (The Open Group)]
* https://csrc.nist.gov/publications/detail/sp/800-64/rev-2/final[Security Considerations in the System Development Life Cycle (NIST)]
* https://cloudsecurityalliance.org/guidance/#_overview[Security Guidance for Critical Areas of Focus in Cloud Computing (CSA)]
* https://www.iso.org/standard/44378.html[Application Security - Part 1: Overview and Concepts, ISO/IEC27034-1:2011 (ISO/IEC)]
* https://www.owasp.org/index.php/Category:OWASP_Top_Ten_Project[OWASP Top 10]
* https://cwe.mitre.org/, https://cve.mitre.org/[Common Weaknesses Enumeration, Common Vulnerability, and Exposures (MITRE)]
